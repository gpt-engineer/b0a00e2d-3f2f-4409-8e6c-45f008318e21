// Fetch news articles from Bild.de
function fetchBildNews() {
  // Implement the code to fetch news articles from Bild.de
}

// Fetch news articles from SVT.se
function fetchSvtNews() {
  // Implement the code to fetch news articles from SVT.se
}

// Fetch news articles from The Guardian
function fetchGuardianNews() {
  // Implement the code to fetch news articles from The Guardian
}

// Display news articles on the website
function displayNews(articles) {
  const newsContainer = document.getElementById("news-container");

  // Clear previous articles
  newsContainer.innerHTML = "";

  // Loop through the articles and create HTML elements to display them
  articles.forEach((article) => {
    const articleElement = document.createElement("div");
    articleElement.classList.add("bg-white", "p-4", "rounded", "shadow");

    const titleElement = document.createElement("h2");
    titleElement.classList.add("text-xl", "font-bold", "mb-2");
    titleElement.textContent = article.title;

    const descriptionElement = document.createElement("p");
    descriptionElement.classList.add("text-gray-600");
    descriptionElement.textContent = article.description;

    const sourceElement = document.createElement("p");
    sourceElement.classList.add("text-gray-500", "text-sm");
    sourceElement.textContent = `Source: ${article.source}`;

    articleElement.appendChild(titleElement);
    articleElement.appendChild(descriptionElement);
    articleElement.appendChild(sourceElement);

    newsContainer.appendChild(articleElement);
  });
}

// Fetch and display news articles from Bild.de by default
fetchBildNews();

// Event listeners for navigation links
document.querySelector("nav ul").addEventListener("click", (event) => {
  event.preventDefault();

  const target = event.target;
  if (target.tagName === "A") {
    const source = target.textContent;

    switch (source) {
      case "Bild.de":
        fetchBildNews();
        break;
      case "SVT.se":
        fetchSvtNews();
        break;
      case "The Guardian":
        fetchGuardianNews();
        break;
      default:
        break;
    }
  }
});
